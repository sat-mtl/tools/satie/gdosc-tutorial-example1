extends GridContainer

onready var sender = $OSCsender
onready var message_box = $MessageEdit
onready var count_box = $SpriteCountBox
onready var scale_slider = $ScaleSlider

func _on_SendMessageButton_pressed():
	if sender.is_started():
		sender.msg_address("/demo/sendString")
		sender.msg_add_string(messageBox.text)
		sender.msg_send()


func _on_CreateSpritesButton_pressed():
	if sender.is_started():
		sender.msg_address("/demo/sendInt")
		sender.msg_add_int(count_box.value)
		sender.msg_send()

func _on_ScaleSpritesButton_pressed():
	if sender.is_started():
		sender.msg_address("/demo/sendFloat")
		sender.msg_add_real(scale_slider.value / 100.0)
		sender.msg_send()
