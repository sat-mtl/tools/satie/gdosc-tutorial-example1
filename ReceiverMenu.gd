extends VBoxContainer


const SPRITE_SPACING = 300
var sprites = []

onready var label = $Label
onready var tween = $Tween


func get_message(msg: OSCmessage):
	match msg.address():
		"/demo/sendString":
			var text = msg.arg(0)
			label.text = text
			label.rect_size = label.get_font("font").get_string_size(label.text)
			tween.interpolate_property(label, "rect_position",
					Vector2(-label.rect_size.x, 0), Vector2(ProjectSettings.get_setting("display/window/size/width"), 0), 3,
					Tween.TRANS_LINEAR, Tween.EASE_IN_OUT)
			tween.start()
		"/demo/sendInt":
			var num = msg.arg(0)
			
			# Detect a negative argument, which means we remove sprites
			var adding = true
			if num < 0:
				adding = false
				num = -num
				
			for i in range(num):
				# Add a sprite
				if adding:
					var logo = preload("res://Logo.tscn")
					var newSprite = logo.instance()
					add_child(newSprite)
					newSprite.position.x = (sprites.size() + 0.5) * SPRITE_SPACING
					sprites.push_back(newSprite)
				# Remove a sprite
				elif not sprites.empty():
					remove_child(sprites.pop_back())
		"/demo/sendFloat":
			var scale = msg.arg(0)
			for sprite in sprites:
				sprite.scale *= scale
