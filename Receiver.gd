extends OSCreceiver


# Declare member variables here. Examples:
# var a = 2
# var b = "text"


# Called when the node enters the scene tree for the first time.
func _ready():
	connect("osc_message_received", get_parent(), "get_message")
	set_process(true)
