extends Panel




onready var main_menu = $MainMenu
onready var receiver_menu = $ReceiverMenu
onready var receiver = $ReceiverMenu/OSCreceiver
onready var sender_menu = $SenderMenu

func _on_ReceiveButton_pressed():
	receiver.start()
	receiver_menu.visible = true
	main_menu.visible = false

func _on_SendButton_pressed():
	sender_menu.visible = true
	main_menu.visible = false
